/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import ejb.IdentificationTypeFacadeLocal;
import entity.IdentificationType;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author lfrivera
 */
@ManagedBean
@SessionScoped
public class managedIdentificationType implements Serializable {
    
    @EJB
    private IdentificationTypeFacadeLocal identificationTypeFace;
    private List<IdentificationType> listIdentificationTypes;
    private IdentificationType identificationType;

    public IdentificationTypeFacadeLocal getIdentificationTypeFace() {
        return identificationTypeFace;
    }

    public void setIdentificationTypeFace(IdentificationTypeFacadeLocal identificationTypeFace) {
        this.identificationTypeFace = identificationTypeFace;
    }

    public List<IdentificationType> getListIdentificationTypes() {
        this.listIdentificationTypes = identificationTypeFace.findAll();
        return listIdentificationTypes;
    }

    public void setListIdentificationTypes(List<IdentificationType> listIdentificationTypes) {
        this.listIdentificationTypes = listIdentificationTypes;
    }

    public IdentificationType getIdentificationType() {
        return identificationType;
    }

    public void setIdentificationType(IdentificationType identificationType) {
        this.identificationType = identificationType;
    }

    @PostConstruct
    public void init() {
        this.identificationType = new IdentificationType();
    }
}
