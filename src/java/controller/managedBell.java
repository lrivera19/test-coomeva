/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import ejb.BellFacadeLocal;
import entity.Bell;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author lfrivera
 */
@ManagedBean
@SessionScoped
public class managedBell implements Serializable {
    
    @EJB
    private BellFacadeLocal bellFace;
    private List<Bell> listBells;
    private Bell bell;

    public BellFacadeLocal getBellFace() {
        return bellFace;
    }

    public void setBellFace(BellFacadeLocal bellFace) {
        this.bellFace = bellFace;
    }

    public List<Bell> getListBells() {
        listBells = this.bellFace.findAll();
        return listBells;
    }

    public void setListBells(List<Bell> listBells) {
        this.listBells = listBells;
    }

    public Bell getBell() {
        return bell;
    }

    public void setBell(Bell bell) {
        this.bell = bell;
    }
    
    @PostConstruct
    public void init() {
        this.bell = new Bell();
    }
}
