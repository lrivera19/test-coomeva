/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import ejb.CustomerFacadeLocal;
import entity.Bell;
import entity.Customer;
import entity.IdentificationType;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author lfrivera
 */
@ManagedBean
@SessionScoped
public class managedCustomer implements Serializable {

    @EJB
    private CustomerFacadeLocal customerFace;
    private List<Customer> listCustomers;
    private Customer customer;
    private Bell bell;
    private IdentificationType identificationType;
    private String msj;

    public void guardar() {
        try {
            this.customer.setIdentificationTypeId(identificationType);
            this.customer.setBellsId(bell);
            this.customerFace.create(customer);
            this.msj = "Registro creado correctamente";
            limpiar();
        } catch (Exception e) {
            this.msj = "Error " + e.getMessage();
        }
        FacesMessage message = new FacesMessage(this.msj);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    public void modificar() {
        try {
            this.customer.setIdentificationTypeId(identificationType);
            this.customer.setBellsId(bell);
            this.customerFace.edit(customer);
            this.msj = "Registro actualizado correctamente";
            limpiar();
        } catch (Exception e) {
            this.msj = "Error " + e.getMessage();
        }
        FacesMessage message = new FacesMessage(this.msj);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void editar(Customer c) {
        this.identificationType.setId(c.getIdentificationTypeId().getId());
        this.bell.setId(c.getBellsId().getId());
        this.customer = c;
    }

    public void limpiar() {
        this.identificationType = new IdentificationType();
        this.bell = new Bell();
        this.customer = new Customer();
    }

    public void eliminar(Customer c) {
        try {
            this.customerFace.remove(c);
            this.msj = "Registro eliminado correctamente";
        } catch (Exception e) {
            this.msj = "Error " + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage message = new FacesMessage(this.msj);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public CustomerFacadeLocal getCustomerFace() {
        return customerFace;
    }

    public void setCustomerFace(CustomerFacadeLocal customerFace) {
        this.customerFace = customerFace;
    }

    public List<Customer> getListCustomers() {
        this.listCustomers = this.customerFace.findAll();
        return listCustomers;
    }

    public void setListCustomers(List<Customer> listCustomers) {
        this.listCustomers = listCustomers;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getMsj() {
        return msj;
    }

    public void setMsj(String msj) {
        this.msj = msj;
    }

    public Bell getBell() {
        return bell;
    }

    public void setBell(Bell bell) {
        this.bell = bell;
    }

    public IdentificationType getIdentificationType() {
        return identificationType;
    }

    public void setIdentificationType(IdentificationType identificationType) {
        this.identificationType = identificationType;
    }

    @PostConstruct
    public void init() {
        this.customer = new Customer();
        this.bell = new Bell();
        this.identificationType = new IdentificationType();
    }
}
