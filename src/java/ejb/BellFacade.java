/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entity.Bell;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author lfrivera
 */
@Stateless
public class BellFacade extends AbstractFacade<Bell> implements BellFacadeLocal {

    @PersistenceContext(unitName = "test-coomevaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BellFacade() {
        super(Bell.class);
    }
    
}
