/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entity.IdentificationType;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author lfrivera
 */
@Stateless
public class IdentificationTypeFacade extends AbstractFacade<IdentificationType> implements IdentificationTypeFacadeLocal {

    @PersistenceContext(unitName = "test-coomevaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public IdentificationTypeFacade() {
        super(IdentificationType.class);
    }
    
}
