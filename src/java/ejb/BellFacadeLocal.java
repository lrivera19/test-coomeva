/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entity.Bell;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author lfrivera
 */
@Local
public interface BellFacadeLocal {

    void create(Bell bell);

    void edit(Bell bell);

    void remove(Bell bell);

    Bell find(Object id);

    List<Bell> findAll();

    List<Bell> findRange(int[] range);

    int count();
    
}
