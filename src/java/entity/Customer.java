/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author lfrivera
 */
@Entity
@Table(name = "customers")
public class Customer implements Serializable {
      
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @JoinColumn(name = "identification_type_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @NotNull
    private IdentificationType identificationTypeId;
    
    @Column(name = "identification_number")
    @NotEmpty   
    @Size(min = 1 , max = 32)
    private String identificationNumber;
    
    @Column(name = "full_name")
    @NotEmpty
    @Size(min = 1 , max = 64)
    private String fullName;

    @Column(name = "cell_phone_number")
    @NotEmpty
    @Size(min = 1 , max = 16)
    private String cellPhoneNumber;

    @Column(name = "email")
    @NotEmpty
    @Email
    @Size(min = 1 , max = 128)
    private String email;

    @Column(name = "address")
    @NotEmpty
    @Size(min = 1 , max = 256)
    private String address;

    @JoinColumn(name = "bells_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @NotNull
    private Bell bellsId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public IdentificationType getIdentificationTypeId() {
        return identificationTypeId;
    }

    public void setIdentificationTypeId(IdentificationType identificationTypeId) {
        this.identificationTypeId = identificationTypeId;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCellPhoneNumber() {
        return cellPhoneNumber;
    }

    public void setCellPhoneNumber(String cellPhoneNumber) {
        this.cellPhoneNumber = cellPhoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Bell getBellsId() {
        return bellsId;
    }

    public void setBellsId(Bell bellsId) {
        this.bellsId = bellsId;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Customer other = (Customer) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", identificationTypeId=" + identificationTypeId + ", identificationNumber=" + identificationNumber + ", fullName=" + fullName + ", cellPhoneNumber=" + cellPhoneNumber + ", email=" + email + ", address=" + address + ", bellsId=" + bellsId + '}';
    }
}
