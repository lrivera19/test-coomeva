package entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2022-07-29T12:07:07")
@StaticMetamodel(IdentificationType.class)
public class IdentificationType_ { 

    public static volatile SingularAttribute<IdentificationType, String> code;
    public static volatile SingularAttribute<IdentificationType, String> name;
    public static volatile SingularAttribute<IdentificationType, Integer> id;

}