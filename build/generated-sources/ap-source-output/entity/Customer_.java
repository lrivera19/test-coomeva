package entity;

import entity.Bell;
import entity.IdentificationType;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2022-07-29T12:07:07")
@StaticMetamodel(Customer.class)
public class Customer_ { 

    public static volatile SingularAttribute<Customer, String> address;
    public static volatile SingularAttribute<Customer, String> cellPhoneNumber;
    public static volatile SingularAttribute<Customer, IdentificationType> identificationTypeId;
    public static volatile SingularAttribute<Customer, String> identificationNumber;
    public static volatile SingularAttribute<Customer, String> fullName;
    public static volatile SingularAttribute<Customer, Integer> id;
    public static volatile SingularAttribute<Customer, Bell> bellsId;
    public static volatile SingularAttribute<Customer, String> email;

}